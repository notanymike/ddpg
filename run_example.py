from pdb import set_trace
import gym
from gym.envs.box2d import CarRacing

from stable_baselines.common.policies import MlpPolicy, CnnPolicy
from stable_baselines.ddpg.policies import FeedForwardPolicy
#from stable_baselines.ddpg.policies import CnnPolicy as CnnPolicyDdpg
from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines import DDPG, PPO2, A2C, ACKTR

#set_trace()

#env = gym.make('CartPole-v1')
env = gym.make('CarRacing-v0'
        ,allow_reverse=False, 
        grayscale=1,
        show_info_panel=0,
        discretize_actions="hard",
        frames_per_state=4,
        max_episode_reward=1,
        )
#env = CarRacing()
env = DummyVecEnv([lambda: env])  # The algorithms require a vectorized environment to run

#model = A2C(CnnPolicy, env, verbose=1, tensorboard_log="./tb/a2c3/", n_steps=1000,
        #max_grad_norm=None)
#model = DDPG(CnnPolicyDdpg, env, verbose=1,render=1, tensorboard_log="./tb/ddpg/", memory_limit=100)
model = PPO2(CnnPolicy, env, verbose=1, tensorboard_log="./tb/ppo2", 
        max_grad_norm=100, 
        n_steps=200,
        )
#model = ACKTR(CnnPolicy, env, verbose=1, tensorboard_log="/.tb/acktr")

for i in range (200):
    obs = env.envs[0].reset()
    for _ in range (1000):
        action, _states = model.predict(obs)
        obs, rewards, dones, info = env.envs[0].step(action)
        if dones == True: env.envs[0].reset()
        env.envs[0].render()

    model.learn(total_timesteps=50000)
    model.save("trained_models/a2c_CarRacing_%i" % (i*50000))
