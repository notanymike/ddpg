from pdb import set_trace
import gym
from gym.envs.box2d import CarRacing

from stable_baselines.common.policies import MlpPolicy, CnnPolicy
from stable_baselines.ddpg.policies import FeedForwardPolicy
from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines import DDPG, PPO2, A2C, ACKTR

env = gym.make('CarRacing-v0'
        ,allow_reverse=False, 
        grayscale=1,
        show_info_panel=0,
        discretize_actions="hard",
        frames_per_state=4,
        #num_lanes=2,
        #num_lanes_changes=3,
        #num_tracks=2,
        )
#env = CarRacing()
env = DummyVecEnv([lambda: env])  # The algorithms require a vectorized environment to run

#model = PPO2(CnnPolicy, env, verbose=1, tensorboard_log="./tb/ppo2", 
#        max_grad_norm=100, 
#        n_steps=200,
#        )

model = PPO2.load("trained_models/a2c_CarRacing_200000")
obs = env.envs[0].reset()
while 1:
    action, _states = model.predict(obs)
    obs, rewards, dones, info = env.envs[0].step(action)
    if dones == True: env.envs[0].reset()
    env.envs[0].render()
