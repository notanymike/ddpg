import gym

from stable_baselines.common.policies import MlpPolicy, CnnPolicy
from stable_baselines.common.vec_env import SubprocVecEnv
from stable_baselines import A2C

# multiprocess environment
n_cpu = 6
env = SubprocVecEnv([lambda: gym.make('CarRacing-v0') for i in range(n_cpu)])

model = A2C(CnnPolicy, env, verbose=1,tensorboard_log="tb/a2c/")
model.learn(total_timesteps=100000000)
model.save("trained_models/a2c_carracing")

del model # remove to demonstrate saving and loading

model = A2C.load("trained_models/a2c_carracing")

obs = env.reset()
while True:
    action, _states = model.predict(obs)
    obs, rewards, dones, info = env.step(action)
    env.render()
