import gym
import gym_gazebo
from gym import wrappers
from baselines.ddpg import ddpg
from pdb import set_trace
from gym.spaces import Box

import os
import time
import sys
import matplotlib.pyplot as plt
import numpy as np
import cv2

try:
    env_param = gym.make("EUFSGazeboEnv-v0")
    outdir = '/tmp/gym_experiments'
    env = gym.wrappers.Monitor(env_param, outdir, force=True)
    env_param.set_v_goal(3)
    env.reset()

    network = 'mlp'
    env.action_space = Box(-1,1,shape=(2,))
    env.observation_space = Box(-1,1,shape=(1,))


    ddpg.learn(network=network, env=env)
except:
    print("Exiting...")
    cv2.destroyAllWindows()
    env.close()
    sys.exit()
